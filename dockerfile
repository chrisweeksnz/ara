FROM ubuntu:rolling

LABEL maintainer="Chris Weeks <chris@weeks.net.nz>" \
      purpose="Ansible Runtime Analysis"

######
# ARA
######
# - https://github.com/ansible-community/ara
# - ~/.ara/server/ansible.sqlite


ENV PATH /root/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

RUN apt -y update && \
    apt -y upgrade && \
    DEBIAN_FRONTEND=noninteractive apt -y install \
           bash \
           libffi-dev \
           libssl-dev \
           python3 \
           python3-crypto \
           python3-dev \
           python3-pip \
           sqlite \
           unzip \
           wget && \
    pip3 install --upgrade \
        ansible \
        ara[server] && \
    mkdir -p /etc/ansible && \
    wget -P /etc/ansible https://raw.githubusercontent.com/ansible/ansible/devel/examples/ansible.cfg && \
    python3 -m ara.setup.env >> ~/.bashrc

EXPOSE 8000
CMD [ "ara-manage", "runserver" ]
