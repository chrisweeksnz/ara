# ara

ara (Ansible Runtime Analysis) is best used with [ara-web](https://gitlab.com/chrisweeksnz/ara-web).

### Bootstrap

```shell
# Start a temp copy of the container
docker run \
  -it --rm \
  --net=host \
  -v ${HOME}/.ara:/root/.ara \
  registry.gitlab.com/chrisweeksnz/ara:latest \
  bash

# Run a playbook
cat > ~/test.yml <<'EOF'
---
- hosts: localhost
  tasks:
  - name: gather facts
    setup:
EOF
ansible-playbook ~/test.yml

# Create the ara super user
ara-manage createsuperuser --username=mysuperuser --email=username@my.domain

# exit the temporary container
exit

# launch a daemonised instance of the container or use compose to start both
# ara and ara-web. In this example, we simply start ara.
docker run \
  -d \
  --net=host \
  --name=ara \
  -v ${HOME}/.ara:/root/.ara \
  registry.gitlab.com/chrisweeksnz/ara:latest
```


Open a browser to http://locahost:8000/admin/. Login with your superuser 
credentials. Create a user for normal activity. 

Open a browser to http://locahost:8000/ to browse the api.


### Other details

```
# if testing ara on a particular interface, something like this is useful:
#ara-manage runserver $(ip addr show type veth | grep inet | sed 's|.*inet ||;s|/.*||'):8000
```